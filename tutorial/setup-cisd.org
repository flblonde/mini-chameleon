#+TITLE: Getting started
#+SUBTITLE: cisd@enseirb-matmeca.bordeaux-inp.fr
#+AUTHOR: Emmanuel Agullo and Mathieu Faverge
#+SETUPFILE: ../include/compose-publish/theme-readtheorginria.setup
#+PROPERTY: header-args:bash :eval no :exports both

* Objectives

The main objectives of this project is to develop by yourself a small linear
algebra library (that includes a GEMM, and a GETRF without pivoting) with the
highest possible performance for:
- the sequential kernels;
- the multi-threaded kernels;
- the MPI only implementation;
- and, as a bonus, the MPI+thread implementation.

All developments will need to be carefully studied and explained in your report
for each of these versions. Comparative studies of the results using R figures
will be asked to show the good performances, and the improvement brought by each
proposed functionality.

As a complement to the report, a presentation of the results will be made. This
presentation will be taken into account for the evaluation of the English class.

* Specific instructions for =enseirb-matmeca= =cisd=:

** Collaborative notes and possible hotfixes for the whole promo

   [[https://link.infini.fr/cisd-2022]]

** Collaborative set up on the =enseirb= =gitlab= instance

Pour travailler en collaboration via le ~gitlab~ de l'~enseirb~ (et nous permettre
de suivre vos avancées), nous vous emandons de travailler sur le dépôt qui vous est fourni:
#+begin_src bash :eval no
TEAMID=X
git clone --recursive git@gitlab.enseirb-matmeca.dev:cisd/is331/team${TEAMID}.git
cd team${TEAMID}
#+end_src

où X doit être remplacé par le uméro de votre équipe.

Pour récupérer le code de travail de base et pouvoir intégrer facilement les
mises à jour, il va falloir l'ajouter en tant que nouvelle remote:

#+begin_src bash :eval no
git remote add subject git@gitlab.inria.fr:solverstack/mini-examples/mini-chameleon.git
git fetch subject
git submodule update --init
#+end_src

Ainsi, le master du dépôt du sujet devrait être identique à celui de votre dépôt
de projet de départ.

#+begin_example
597aaf7 (HEAD -> master, teamX/master, subject/master) Channels with new mini-chameleon readily available
#+end_example

À ce stade, tout est configuré pour que vous puissiez travailler sur le projet.
En cas de mise à jour du sujet, il faudra synchroniser à nouveau votre master
avec celui du dépôt. En cas de soucis pour faire cette démarche,
n'hésitez pas à nous demander. Mais surtout:

_ Assurez-vous que le dépôt ne contient aucunes modifications locales avant de faire
la manipulation et que vous êtes bien positionné sur le dernier =master= de
votre dépôt. _

* Non specific instructions

The rest of the instructions are non specific to =cisd= and can be retrieved [[../README.org][on
the main page of the project]].
